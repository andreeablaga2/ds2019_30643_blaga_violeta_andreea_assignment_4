package assignment1_sd.a1;

//import assignment1_sd.a1.services.DispenserService;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.TimeZone;

@SpringBootApplication
//@EnableAutoConfiguration
public class A1Application {

	public static void main(String[] args) throws IOException, InterruptedException{

		//TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        SpringApplication.run(A1Application.class, args);

//		System.out.println("starting GRPC Server");
//		Server server = ServerBuilder.forPort(9090).addService(new DispenserService()).build();
//
//		server.start();
//
//		System.out.println("server started at "+ server.getPort());
//
//		//SpringApplication.run(A1Application.class, args);
//		server.awaitTermination();
	}

//	@Component
//	class ApplicationRunnerBean implements ApplicationRunner {
//
//		@Autowired
//		DispenserService dispenserService;
//
//		@Override
//		public void run(ApplicationArguments args) throws Exception {
//			System.out.println("starting GRPC Server");
//			Server server = ServerBuilder.forPort(9090).addService(dispenserService).build();
//
//			server.start();
//
//			System.out.println("server started at "+ server.getPort());
//
//			server.awaitTermination();
//		}
//	}

}
