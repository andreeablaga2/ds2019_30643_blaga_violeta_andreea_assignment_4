package assignment1_sd.a1.consumer;

//import assignment1_sd.a1.entities.MonitoredData;
//import assignment1_sd.a1.entities.User;
//import assignment1_sd.a1.services.MonitoredDataService;
//import assignment1_sd.a1.services.UserService;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.messaging.simp.SimpMessagingTemplate;
//import org.springframework.stereotype.Component;
//
//import java.io.IOException;
//import java.time.temporal.ChronoUnit;
//
//@Component
//public class Consumer {
//
//    private final SimpMessagingTemplate messagingTemplate;
//
////    @Autowired
////    private ObjectMapper mapper = new ObjectMapper();
//
////    @Autowired
////    private UserService userService;
//
//    @Autowired
//    private MonitoredDataService monitoredDataService;
//
//    public Consumer( SimpMessagingTemplate messagingTemplate) {
//        this.messagingTemplate = messagingTemplate;
//    }
//
//    public void checkRules(MonitoredData data) {
//        //r1
//        if (data.getActivity().equals("Sleeping") && (ChronoUnit.HOURS.between(data.getStart(), data.getEnd()) > 4)) {
//            messagingTemplate.convertAndSend("/topic/events",data);
//            System.out.println("Sleep period longer than 12 hours");
//        }
//        //r2
//        else if (data.getActivity().equals("Leaving\t") && (ChronoUnit.HOURS.between(data.getStart(), data.getEnd()) > 0)) {
//            System.out.println("Leaving activity (outdoor) is longer than 12 hours");
//            messagingTemplate.convertAndSend("/topic/events",data);
//        }
//        //r3
//        else if ((data.getActivity().equals("Toileting\t") || data.getActivity().equals("Showering\t")) && (ChronoUnit.HOURS.between(data.getStart(), data.getEnd()) > 0)) {
//            System.out.println("Period spent in bathroom is longer than 1 hour");
//            messagingTemplate.convertAndSend("/topic/events",data);
//        }
//    }
//
//    @RabbitListener(queues="${jsa.rabbitmq.queue}", containerFactory="jsaFactory")
//    public void receivedMessage(MonitoredData data){
//        System.out.println("Received message: " + data.toString());
//        //inserez in bd
//        //monitoredDataService.create(data);
//        checkRules(data);
//    }
//
//
//        //     for (MonitoredData m : lista) {
////        //r1
////        if(m.getActivity().equals("Sleeping") && (ChronoUnit.HOURS.between(m.getStart(),m.getEnd())>12)){
////            System.out.println(m.getStart()+"-------"+m.getEnd());
////            System.out.print("           DOARMEEEE");
////        }
////        //r2
////        else if(m.getActivity().equals("Leaving\t") && (ChronoUnit.HOURS.between(m.getStart(),m.getEnd())>1)) {
////            System.out.println(m.getStart()+"-------"+m.getEnd());
////            System.out.print("           IESEEEE");
////        }
////        //r3
////        else if ((m.getActivity().equals("Toileting\t") || m.getActivity().equals("Showering\t")) && (ChronoUnit.HOURS.between(m.getStart(),m.getEnd())>0)){
////            System.out.println(m.getStart()+"-------"+m.getEnd());
////            System.out.print("           LA BAIE");
////        }
////    }
//
//
////    public void recievedMessage(String msg) {
////        System.out.println("Recieved Message: " + msg);
////    }
//}

