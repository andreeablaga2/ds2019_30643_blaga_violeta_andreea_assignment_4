package assignment1_sd.a1.controller;

import assignment1_sd.a1.entities.MonitoredData;
import assignment1_sd.a1.services.MonitoredDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/doctor")
public class DoctorController {
    @Autowired
    private MonitoredDataService monitoredDataService;

    @GetMapping("/1/history")
    public List<MonitoredData> viewHistory(){
        return monitoredDataService.findAll();
    }
}
