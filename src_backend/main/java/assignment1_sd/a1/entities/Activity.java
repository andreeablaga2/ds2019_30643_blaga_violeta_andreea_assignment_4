package assignment1_sd.a1.entities;

import javax.persistence.*;

@Entity
@Table(name="activities")
public class Activity {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "activity_id")
    private Integer activityID;

    @Column(name="start")
    private String start_time;

    @Column(name="end")
    private String end_time;

    @Column(name="name")
    private String name;

    public Activity(Integer activityID, String start_time, String end_time, String name) {
        this.activityID = activityID;
        this.start_time = start_time;
        this.end_time = end_time;
        this.name = name;
    }

    public Integer getActivityID() {
        return activityID;
    }

    public void setActivityID(Integer activityID) {
        this.activityID = activityID;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
