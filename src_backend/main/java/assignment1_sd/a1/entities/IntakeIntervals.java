package assignment1_sd.a1.entities;


import javax.persistence.*;

@Entity
@Table(name="intake_intervals")
public class IntakeIntervals {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "intake_id")
    private Integer intakeID;

//    @OneToMany(fetch=FetchType.LAZY,  mappedBy = "intakeInterval")
//    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
//    @JoinColumn(name = "intake_id")
    @ManyToOne
    @JoinColumn(name = "medplan_id")
    private MedicationPlan medplan;

   // @OneToMany(fetch=FetchType.LAZY,  mappedBy = "intakeInterval")
//   @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
//   @JoinColumn(name = "intake_id")
   @ManyToOne
   @JoinColumn(name = "medication_id")
    private Medication med;

    @Column(name="start_time")
    private Integer start_time;

    @Column(name="end_time")
    private Integer end_time;

    public IntakeIntervals(){

    }

    public IntakeIntervals(Integer intakeID, MedicationPlan medplan, Medication med, Integer start_time, Integer end_time) {
        this.intakeID = intakeID;
        this.medplan = medplan;
        this.med = med;
        this.start_time = start_time;
        this.end_time = end_time;
    }

    public Integer getIntakeID() {
        return intakeID;
    }

    public void setIntakeID(Integer intakeID) {
        this.intakeID = intakeID;
    }

    public MedicationPlan getMedplan() {
        return medplan;
    }

    public void setMedplan(MedicationPlan medplan) {
        this.medplan = medplan;
    }

    public Medication getMed() {
        return med;
    }

    public void setMed(Medication med) {
        this.med = med;
    }

    public Integer getStart_time() {
        return start_time;
    }

    public void setStart_time(Integer start_time) {
        this.start_time = start_time;
    }

    public Integer getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Integer end_time) {
        this.end_time = end_time;
    }
}
