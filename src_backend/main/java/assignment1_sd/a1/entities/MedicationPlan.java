package assignment1_sd.a1.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="medication_plan")
public class MedicationPlan {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "medplan_id")
    private Integer medplan_id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="patient_id", nullable=false)
    private Patient patient;

//    @ManyToMany(cascade = {
//            CascadeType.PERSIST,
//            CascadeType.MERGE
//    })
//    @JoinTable(name = "medplan_med",
//            joinColumns = @JoinColumn(name = "medplan_id"),
//            inverseJoinColumns = @JoinColumn(name = "medication_id")
//    )
//    private List<Medication> meds = new ArrayList<>();   //+met de add,remove

//    @Column(name="intake_intervals")
//    private String intakeIntervals;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name="intake_id", nullable=true)
//    private IntakeIntervals intakeInterval;

    //@OneToMany(mappedBy = "medplan")
    //private Set<IntakeIntervals> intakeIntervals;

    @Column(name="per_of_treatment")
    private String periodOfTreatment;

    public MedicationPlan() {
    }

//    public MedicationPlan(Integer medplan_id, Patient patient, List<Medication> meds, String intakeIntervals, String periodOfTreatment) {
//        this.medplan_id = medplan_id;
//        this.patient = patient;
//        this.meds = meds;
//        this.intakeIntervals = intakeIntervals;
//        this.periodOfTreatment = periodOfTreatment;
//    }


    public MedicationPlan(Integer medplan_id, Patient patient, String periodOfTreatment) {
        this.medplan_id = medplan_id;
        this.patient = patient;
        this.periodOfTreatment = periodOfTreatment;
    }

    public Integer getMedplan_id() {
        return medplan_id;
    }

    public void setMedplan_id(Integer medplan_id) {
        this.medplan_id = medplan_id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

//    public List<Medication> getMeds() {
//        return meds;
//    }
//
//    public void setMeds(List<Medication> meds) {
//        this.meds = meds;
//    }
//
//    public String getIntakeIntervals() {
//        return intakeIntervals;
//    }
//
//    public void setIntakeIntervals(String intakeIntervals) {
//        this.intakeIntervals = intakeIntervals;
//    }

    public String getPeriodOfTreatment() {
        return periodOfTreatment;
    }

    public void setPeriodOfTreatment(String periodOfTreatment) {
        this.periodOfTreatment = periodOfTreatment;
    }

//    public void addMed(Medication m){
//        meds.add(m);
//        m.getMedplans().add(this);
//    }
//
//    public void removeMed(Medication m){
//        meds.remove(m);
//        m.getMedplans().remove(this);
//    }
}
