package assignment1_sd.a1.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: med.proto")
public final class dispenserGrpc {

  private dispenserGrpc() {}

  public static final String SERVICE_NAME = "assignment1_sd.a1.grpc.dispenser";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<assignment1_sd.a1.grpc.Med.LoginRequest,
      assignment1_sd.a1.grpc.Med.APIResponse> getLoginMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "login",
      requestType = assignment1_sd.a1.grpc.Med.LoginRequest.class,
      responseType = assignment1_sd.a1.grpc.Med.APIResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<assignment1_sd.a1.grpc.Med.LoginRequest,
      assignment1_sd.a1.grpc.Med.APIResponse> getLoginMethod() {
    io.grpc.MethodDescriptor<assignment1_sd.a1.grpc.Med.LoginRequest, assignment1_sd.a1.grpc.Med.APIResponse> getLoginMethod;
    if ((getLoginMethod = dispenserGrpc.getLoginMethod) == null) {
      synchronized (dispenserGrpc.class) {
        if ((getLoginMethod = dispenserGrpc.getLoginMethod) == null) {
          dispenserGrpc.getLoginMethod = getLoginMethod = 
              io.grpc.MethodDescriptor.<assignment1_sd.a1.grpc.Med.LoginRequest, assignment1_sd.a1.grpc.Med.APIResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "assignment1_sd.a1.grpc.dispenser", "login"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  assignment1_sd.a1.grpc.Med.LoginRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  assignment1_sd.a1.grpc.Med.APIResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new dispenserMethodDescriptorSupplier("login"))
                  .build();
          }
        }
     }
     return getLoginMethod;
  }

  private static volatile io.grpc.MethodDescriptor<assignment1_sd.a1.grpc.Med.Empty,
      assignment1_sd.a1.grpc.Med.APIResponse> getLogoutMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "logout",
      requestType = assignment1_sd.a1.grpc.Med.Empty.class,
      responseType = assignment1_sd.a1.grpc.Med.APIResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<assignment1_sd.a1.grpc.Med.Empty,
      assignment1_sd.a1.grpc.Med.APIResponse> getLogoutMethod() {
    io.grpc.MethodDescriptor<assignment1_sd.a1.grpc.Med.Empty, assignment1_sd.a1.grpc.Med.APIResponse> getLogoutMethod;
    if ((getLogoutMethod = dispenserGrpc.getLogoutMethod) == null) {
      synchronized (dispenserGrpc.class) {
        if ((getLogoutMethod = dispenserGrpc.getLogoutMethod) == null) {
          dispenserGrpc.getLogoutMethod = getLogoutMethod = 
              io.grpc.MethodDescriptor.<assignment1_sd.a1.grpc.Med.Empty, assignment1_sd.a1.grpc.Med.APIResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "assignment1_sd.a1.grpc.dispenser", "logout"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  assignment1_sd.a1.grpc.Med.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  assignment1_sd.a1.grpc.Med.APIResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new dispenserMethodDescriptorSupplier("logout"))
                  .build();
          }
        }
     }
     return getLogoutMethod;
  }

  private static volatile io.grpc.MethodDescriptor<assignment1_sd.a1.grpc.Med.PatientRequest,
      assignment1_sd.a1.grpc.Med.DispenserAPIResponse> getDownloadMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "download",
      requestType = assignment1_sd.a1.grpc.Med.PatientRequest.class,
      responseType = assignment1_sd.a1.grpc.Med.DispenserAPIResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<assignment1_sd.a1.grpc.Med.PatientRequest,
      assignment1_sd.a1.grpc.Med.DispenserAPIResponse> getDownloadMethod() {
    io.grpc.MethodDescriptor<assignment1_sd.a1.grpc.Med.PatientRequest, assignment1_sd.a1.grpc.Med.DispenserAPIResponse> getDownloadMethod;
    if ((getDownloadMethod = dispenserGrpc.getDownloadMethod) == null) {
      synchronized (dispenserGrpc.class) {
        if ((getDownloadMethod = dispenserGrpc.getDownloadMethod) == null) {
          dispenserGrpc.getDownloadMethod = getDownloadMethod = 
              io.grpc.MethodDescriptor.<assignment1_sd.a1.grpc.Med.PatientRequest, assignment1_sd.a1.grpc.Med.DispenserAPIResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "assignment1_sd.a1.grpc.dispenser", "download"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  assignment1_sd.a1.grpc.Med.PatientRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  assignment1_sd.a1.grpc.Med.DispenserAPIResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new dispenserMethodDescriptorSupplier("download"))
                  .build();
          }
        }
     }
     return getDownloadMethod;
  }

  private static volatile io.grpc.MethodDescriptor<assignment1_sd.a1.grpc.Med.Request,
      assignment1_sd.a1.grpc.Med.APIResponse> getNotifyMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "notify",
      requestType = assignment1_sd.a1.grpc.Med.Request.class,
      responseType = assignment1_sd.a1.grpc.Med.APIResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<assignment1_sd.a1.grpc.Med.Request,
      assignment1_sd.a1.grpc.Med.APIResponse> getNotifyMethod() {
    io.grpc.MethodDescriptor<assignment1_sd.a1.grpc.Med.Request, assignment1_sd.a1.grpc.Med.APIResponse> getNotifyMethod;
    if ((getNotifyMethod = dispenserGrpc.getNotifyMethod) == null) {
      synchronized (dispenserGrpc.class) {
        if ((getNotifyMethod = dispenserGrpc.getNotifyMethod) == null) {
          dispenserGrpc.getNotifyMethod = getNotifyMethod = 
              io.grpc.MethodDescriptor.<assignment1_sd.a1.grpc.Med.Request, assignment1_sd.a1.grpc.Med.APIResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "assignment1_sd.a1.grpc.dispenser", "notify"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  assignment1_sd.a1.grpc.Med.Request.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  assignment1_sd.a1.grpc.Med.APIResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new dispenserMethodDescriptorSupplier("notify"))
                  .build();
          }
        }
     }
     return getNotifyMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static dispenserStub newStub(io.grpc.Channel channel) {
    return new dispenserStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static dispenserBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new dispenserBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static dispenserFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new dispenserFutureStub(channel);
  }

  /**
   */
  public static abstract class dispenserImplBase implements io.grpc.BindableService {

    /**
     */
    public void login(assignment1_sd.a1.grpc.Med.LoginRequest request,
        io.grpc.stub.StreamObserver<assignment1_sd.a1.grpc.Med.APIResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getLoginMethod(), responseObserver);
    }

    /**
     */
    public void logout(assignment1_sd.a1.grpc.Med.Empty request,
        io.grpc.stub.StreamObserver<assignment1_sd.a1.grpc.Med.APIResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getLogoutMethod(), responseObserver);
    }

    /**
     */
    public void download(assignment1_sd.a1.grpc.Med.PatientRequest request,
        io.grpc.stub.StreamObserver<assignment1_sd.a1.grpc.Med.DispenserAPIResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getDownloadMethod(), responseObserver);
    }

    /**
     */
    public void notify(assignment1_sd.a1.grpc.Med.Request request,
        io.grpc.stub.StreamObserver<assignment1_sd.a1.grpc.Med.APIResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getNotifyMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getLoginMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                assignment1_sd.a1.grpc.Med.LoginRequest,
                assignment1_sd.a1.grpc.Med.APIResponse>(
                  this, METHODID_LOGIN)))
          .addMethod(
            getLogoutMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                assignment1_sd.a1.grpc.Med.Empty,
                assignment1_sd.a1.grpc.Med.APIResponse>(
                  this, METHODID_LOGOUT)))
          .addMethod(
            getDownloadMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                assignment1_sd.a1.grpc.Med.PatientRequest,
                assignment1_sd.a1.grpc.Med.DispenserAPIResponse>(
                  this, METHODID_DOWNLOAD)))
          .addMethod(
            getNotifyMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                assignment1_sd.a1.grpc.Med.Request,
                assignment1_sd.a1.grpc.Med.APIResponse>(
                  this, METHODID_NOTIFY)))
          .build();
    }
  }

  /**
   */
  public static final class dispenserStub extends io.grpc.stub.AbstractStub<dispenserStub> {
    private dispenserStub(io.grpc.Channel channel) {
      super(channel);
    }

    private dispenserStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected dispenserStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new dispenserStub(channel, callOptions);
    }

    /**
     */
    public void login(assignment1_sd.a1.grpc.Med.LoginRequest request,
        io.grpc.stub.StreamObserver<assignment1_sd.a1.grpc.Med.APIResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getLoginMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void logout(assignment1_sd.a1.grpc.Med.Empty request,
        io.grpc.stub.StreamObserver<assignment1_sd.a1.grpc.Med.APIResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getLogoutMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void download(assignment1_sd.a1.grpc.Med.PatientRequest request,
        io.grpc.stub.StreamObserver<assignment1_sd.a1.grpc.Med.DispenserAPIResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDownloadMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void notify(assignment1_sd.a1.grpc.Med.Request request,
        io.grpc.stub.StreamObserver<assignment1_sd.a1.grpc.Med.APIResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getNotifyMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class dispenserBlockingStub extends io.grpc.stub.AbstractStub<dispenserBlockingStub> {
    private dispenserBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private dispenserBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected dispenserBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new dispenserBlockingStub(channel, callOptions);
    }

    /**
     */
    public assignment1_sd.a1.grpc.Med.APIResponse login(assignment1_sd.a1.grpc.Med.LoginRequest request) {
      return blockingUnaryCall(
          getChannel(), getLoginMethod(), getCallOptions(), request);
    }

    /**
     */
    public assignment1_sd.a1.grpc.Med.APIResponse logout(assignment1_sd.a1.grpc.Med.Empty request) {
      return blockingUnaryCall(
          getChannel(), getLogoutMethod(), getCallOptions(), request);
    }

    /**
     */
    public assignment1_sd.a1.grpc.Med.DispenserAPIResponse download(assignment1_sd.a1.grpc.Med.PatientRequest request) {
      return blockingUnaryCall(
          getChannel(), getDownloadMethod(), getCallOptions(), request);
    }

    /**
     */
    public assignment1_sd.a1.grpc.Med.APIResponse notify(assignment1_sd.a1.grpc.Med.Request request) {
      return blockingUnaryCall(
          getChannel(), getNotifyMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class dispenserFutureStub extends io.grpc.stub.AbstractStub<dispenserFutureStub> {
    private dispenserFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private dispenserFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected dispenserFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new dispenserFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<assignment1_sd.a1.grpc.Med.APIResponse> login(
        assignment1_sd.a1.grpc.Med.LoginRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getLoginMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<assignment1_sd.a1.grpc.Med.APIResponse> logout(
        assignment1_sd.a1.grpc.Med.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getLogoutMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<assignment1_sd.a1.grpc.Med.DispenserAPIResponse> download(
        assignment1_sd.a1.grpc.Med.PatientRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getDownloadMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<assignment1_sd.a1.grpc.Med.APIResponse> notify(
        assignment1_sd.a1.grpc.Med.Request request) {
      return futureUnaryCall(
          getChannel().newCall(getNotifyMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_LOGIN = 0;
  private static final int METHODID_LOGOUT = 1;
  private static final int METHODID_DOWNLOAD = 2;
  private static final int METHODID_NOTIFY = 3;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final dispenserImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(dispenserImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_LOGIN:
          serviceImpl.login((assignment1_sd.a1.grpc.Med.LoginRequest) request,
              (io.grpc.stub.StreamObserver<assignment1_sd.a1.grpc.Med.APIResponse>) responseObserver);
          break;
        case METHODID_LOGOUT:
          serviceImpl.logout((assignment1_sd.a1.grpc.Med.Empty) request,
              (io.grpc.stub.StreamObserver<assignment1_sd.a1.grpc.Med.APIResponse>) responseObserver);
          break;
        case METHODID_DOWNLOAD:
          serviceImpl.download((assignment1_sd.a1.grpc.Med.PatientRequest) request,
              (io.grpc.stub.StreamObserver<assignment1_sd.a1.grpc.Med.DispenserAPIResponse>) responseObserver);
          break;
        case METHODID_NOTIFY:
          serviceImpl.notify((assignment1_sd.a1.grpc.Med.Request) request,
              (io.grpc.stub.StreamObserver<assignment1_sd.a1.grpc.Med.APIResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class dispenserBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    dispenserBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return assignment1_sd.a1.grpc.Med.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("dispenser");
    }
  }

  private static final class dispenserFileDescriptorSupplier
      extends dispenserBaseDescriptorSupplier {
    dispenserFileDescriptorSupplier() {}
  }

  private static final class dispenserMethodDescriptorSupplier
      extends dispenserBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    dispenserMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (dispenserGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new dispenserFileDescriptorSupplier())
              .addMethod(getLoginMethod())
              .addMethod(getLogoutMethod())
              .addMethod(getDownloadMethod())
              .addMethod(getNotifyMethod())
              .build();
        }
      }
    }
    return result;
  }
}
