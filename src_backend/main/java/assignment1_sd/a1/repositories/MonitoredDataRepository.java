package assignment1_sd.a1.repositories;

import assignment1_sd.a1.entities.MonitoredData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MonitoredDataRepository extends JpaRepository<MonitoredData, Integer> {

}
