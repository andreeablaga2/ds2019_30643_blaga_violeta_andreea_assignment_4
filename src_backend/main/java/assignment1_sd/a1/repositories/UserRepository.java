package assignment1_sd.a1.repositories;

import assignment1_sd.a1.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, Integer> {
    @Query("SELECT user FROM User user WHERE user.username= :username AND user.password= :password")
    public User findByUsernameAndPassword(String username, String password);


}
