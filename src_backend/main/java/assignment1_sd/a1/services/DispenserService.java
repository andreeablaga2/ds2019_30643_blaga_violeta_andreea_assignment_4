package assignment1_sd.a1.services;

//import assignment1_sd.a1.entities.IntakeIntervals;
//import assignment1_sd.a1.grpc.Med;
//import assignment1_sd.a1.grpc.Med.LoginRequest;
//import assignment1_sd.a1.grpc.Med.APIResponse;
//import assignment1_sd.a1.grpc.Med.Empty;
//import assignment1_sd.a1.grpc.Med.PatientRequest;
//import assignment1_sd.a1.grpc.Med.DispenserAPIResponse;
//import assignment1_sd.a1.grpc.Med.Request;
//import assignment1_sd.a1.grpc.Med.MedPlan;
//import assignment1_sd.a1.grpc.dispenserGrpc.dispenserImplBase;
//import assignment1_sd.a1.repositories.DispenserRepository;
//import io.grpc.stub.StreamObserver;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.lognet.springboot.grpc.GRpcService;
//
//import java.util.ArrayList;
//import java.util.List;
//
////@Service
//@GRpcService
//public class DispenserService extends dispenserImplBase {
//
//    @Autowired
//    DispenserRepository dispenserRepository;
//
//    @Override
//    public void login(LoginRequest request, StreamObserver<APIResponse> responseObserver) {
//        System.out.println("Inside login");
//
//        String username = request.getUsername();
//        String password = request.getPassword();
//
//        APIResponse.Builder response = APIResponse.newBuilder();
//        if(username.equals(password)) {
//
//            // return success message
//
//            response.setResponseCode(0).setResponsemessage("SUCCESS");
//
//        }
//        else {
//            //return failure message
//
//            response.setResponseCode(100).setResponsemessage("INVALID PASSWORD");
//        }
//
//        responseObserver.onNext(response.build()); //sending data back to the client
//        responseObserver.onCompleted(); //close the call
//    }
//
//    @Override
//    public void logout(Empty request, StreamObserver<APIResponse> responseObserver) {
//
//    }
//
//    @Override
//    public void download(PatientRequest request, StreamObserver<DispenserAPIResponse> responseObserver) {
////        List<IntakeIntervals> intakeIntervals = dispenserRepository.findAll();
////
////        List<IntakeIntervals> medplans = new ArrayList<>();
////
////        for(IntakeIntervals intake: intakeIntervals){
////          if(( intake.getMedplan().getPatient().getUserID() )==request.getId()){
////           // if(intake.getMedplan().getPatient().getUserID()==1){
////                medplans.add(intake);
////            }
////        }
//
//        List<IntakeIntervals> medplans = dispenserRepository.findAll();
//
//        DispenserAPIResponse dispenserAPIResponse = DispenserAPIResponse.newBuilder().build();
//
//        for(IntakeIntervals medplan: medplans){
//
//            if(medplan.getMedplan().getPatient().getUserID()==request.getId()) {
//                MedPlan m = MedPlan.newBuilder()
//                        .setStart(medplan.getStart_time())
//                        .setEnd(medplan.getEnd_time())
//                        .setName(medplan.getMed().getName())
//                        .build();
//                dispenserAPIResponse = DispenserAPIResponse.newBuilder(dispenserAPIResponse).addDispenserAPIresponse(m).build();
//            }
//        }
//
//        responseObserver.onNext(dispenserAPIResponse);
//        responseObserver.onCompleted();
//    }
//
//    @Override
//    public void notify(Request request, StreamObserver<APIResponse> responseObserver) {
//        System.out.println("inside notif");
//        APIResponse.Builder response = APIResponse.newBuilder();
//        response.setResponseCode(0).setResponsemessage("Patient with id: " + Integer.parseInt(request.getMessage()) + " has taken the medication");
//        responseObserver.onNext(response.build());
//        responseObserver.onCompleted();
//    }
//}
