package assignment1_sd.a1.services;

import assignment1_sd.a1.entities.Medication;
import assignment1_sd.a1.repositories.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class MedicationService {
    @Autowired
    private MedicationRepository medRepository;

    public Integer create(Medication medication){
        Medication newmed = medRepository.save(medication);

        return newmed.getMedicationID();
    }

    public List<Medication> findAll() {
        List<Medication> meds = medRepository.findAll();
        return meds;
    }

    public Medication findMedicationById(int medicationID) {
        Optional<Medication> med = medRepository.findById(medicationID);
        if (med==null) {
            System.out.println("Medication does not exist");
            return null;
        }
        Medication m = new Medication();
        m.setMedicationID(med.get().getMedicationID());
        m.setName(med.get().getName());
        m.setSideEffects(med.get().getSideEffects());
        m.setDosage(med.get().getDosage());

        return m;
    }

    public Medication update(Medication medication, int medicationID) {
        Optional<Medication> med = medRepository.findById(medicationID);
        if (med==null) {
            System.out.println("Medication does not exist");
            return null;
        }
        medication.setMedicationID(medicationID);
        medRepository.save(medication);
        return medication;
    }

    public List<Medication> delete(int medicationID){
        medRepository.deleteById(medicationID);
        List<Medication> all = medRepository.findAll();
        return all;
    }

}
