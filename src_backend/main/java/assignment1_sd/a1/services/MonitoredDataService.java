package assignment1_sd.a1.services;

import assignment1_sd.a1.entities.MonitoredData;
import assignment1_sd.a1.repositories.MonitoredDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MonitoredDataService {
    @Autowired
    private MonitoredDataRepository monitoredDataRepository;

    public void create(MonitoredData monitoredData){
        MonitoredData newdata = monitoredDataRepository.save(monitoredData);
        System.out.println("Data inserted!");
        //return newdata;
    }

    public List<MonitoredData> findAll() {
        List<MonitoredData> data = monitoredDataRepository.findAll();
        return data;
    }
}
