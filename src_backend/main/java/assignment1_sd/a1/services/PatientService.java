package assignment1_sd.a1.services;

import assignment1_sd.a1.entities.Medication;
import assignment1_sd.a1.entities.MedicationPlan;
import assignment1_sd.a1.entities.Patient;
import assignment1_sd.a1.repositories.MedicationPlanRepository;
import assignment1_sd.a1.repositories.MedicationRepository;
import assignment1_sd.a1.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PatientService {
    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private MedicationPlanRepository medplanRepository;

    @Autowired
    private MedicationRepository medicationRepository;

    public Integer create(Patient patient){
        Patient newp = patientRepository.save(patient);

        return newp.getUserID();
    }

    public List<Patient> findAll() {
        List<Patient> all = patientRepository.findAll();
        return all;
    }

    public Patient findPatientByID(int patientID) {
        Optional<Patient> patient = patientRepository.findById(patientID);
        if(patient==null){
            System.out.println("Patient does not exist");
            return null;
        }

        Patient p = new Patient();
        p.setUserID(patient.get().getUserID());
        p.setUsername(patient.get().getUsername());
        p.setPassword(patient.get().getPassword());
        p.setType(patient.get().getType());
        p.setName(patient.get().getName());
        p.setBirthDate(patient.get().getBirthDate());
        p.setGender(patient.get().getGender());
        p.setAddress(patient.get().getAddress());
        p.setMedicalRecord(patient.get().getMedicalRecord());
        //p.setCaregiver(patient.get().getCaregiver());

        return p;

    }

    public Patient update(Patient patient, int patientID) {
        Optional<Patient> p = patientRepository.findById(patientID);
        if (p==null) {
            System.out.println("Patient does not exist");
            return null;
        }
        patient.setUserID(patientID);
        patientRepository.save(patient);
        return patient;
    }

    public List<Patient> delete(int patientID){
        patientRepository.deleteById(patientID);
        List<Patient> all = patientRepository.findAll();
        return all;
    }

    //view account details
//    public Patient viewAccountDetails(int patientID){
//        Optional<Patient> patient = patientRepository.findById(patientID);
//        if (patient==null) {
//            System.out.println("Patient does not exist");
//            return null;
//        }
//        if(patient.get().getType() != 1){
//            System.out.println("Only the patient is allowed to view its account details");
//            return null;
//        }
////
////        Patient p = new Patient(patient.get().getUsername(),patient.get().getPassword(),patient.get().getName(),patient.get().getBirthDate(),patient.get().getGender(), patient.get().getAddress(), patient.get().getMedicalRecord());
//////        p.setUserID(patient.get().getUserID());
//////        p.setUsername(patient.get().getUsername());
//////        p.setPassword(patient.get().getPassword());
//////        p.setType(patient.get().getType());
//////        p.setName(patient.get().getName());
//////        p.setBirthDate(patient.get().getBirthDate());
//////        p.setGender(patient.get().getGender());
//////        p.setAddress(patient.get().getAddress());
//////        p.setMedicalRecord(patient.get().getMedicalRecord());
////        return p;
//    }

    //create medication plan
    public MedicationPlan addMedPlan(int patientID, int medicationID, MedicationPlan medicationPlan){
        Optional<Patient> patient = patientRepository.findById(patientID);
        if (patient==null) {
            System.out.println("Patient does not exist");
            return null;
        }

        Optional<Medication> medication = medicationRepository.findById(medicationID);
        if (medication==null) {
            System.out.println("Medication does not exist");
            return null;
        }

        MedicationPlan medplan = new MedicationPlan();
        medplan.setPeriodOfTreatment(medicationPlan.getPeriodOfTreatment());
        //medplan.setIntakeIntervals(medicationPlan.getIntakeIntervals());
        medplan.setPatient(patient.get());

       // medplan.getMeds().add(medication.get());

        return medplanRepository.save(medplan);
    }

    //view medication plans???
    public List<MedicationPlan> viewMedPlans(int patientID){
        List<MedicationPlan> medicationPlans = medplanRepository.findAll();
        List<MedicationPlan> patientsmedplans = new ArrayList<>();
        for(MedicationPlan m: medicationPlans){
            if(m.getPatient().getUserID()==patientID){
                patientsmedplans.add(m);
            }
        }
        return patientsmedplans;
    }
}
