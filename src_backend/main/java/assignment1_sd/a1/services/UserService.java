package assignment1_sd.a1.services;

import assignment1_sd.a1.entities.User;
import assignment1_sd.a1.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRepository usrRepository;

    public int login(User user){
        User usr = usrRepository.findByUsernameAndPassword(user.getUsername(),user.getPassword());
        if (usr == null) {
            System.out.println("You need to register first!");
            return 0;
        }
        else{
            System.out.println("Successfully logged in!");
            return usr.getType();
        }
    }

}
