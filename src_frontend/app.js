import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';
import Persons from './person-data/person/persons'
import Patients from './patient-data/patient/patients'
import Caregivers from './caregiver-data/caregiver/caregivers'
import LoginForm from './pages/LoginForm'
import Medication from './medication-data/medication/medication'
import Doctor from './pages/doctor'
import History from './pages/history'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';

let enums = require('./commons/constants/enums');

class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <NavigationBar />
                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Home/>}
                        />

                        <Route
                            exact
                            path='/persons'
                            render={() => <Persons/>}
                        />

                        <Route
                            exact
                            path='/patients'
                            render={() => <Patients/>}
                        />

                        <Route
                            exact
                            path='/caregivers'
                            render={() => <Caregivers/>}
                        />

                        <Route 
                            exact
                            path="/login"
                            render={() => <LoginForm/>}
                        />

                        <Route 
                            exact
                            path="/medication"
                            render={() => <Medication/>}
                        />

                        <Route 
                            exact
                            path="/doctor"
                            render={() => <Doctor/>}
                        />

                        <Route 
                            exact
                            path="/history"
                            render={() => <History/>}
                        /> 

                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
