import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router'
import axios from "axios";

class LoginForm extends Component {
    constructor() {
        super();

        this.state = {
            username: '',
            password: '',
            login: false,
            role: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        let target = e.target;
        let value = target.type === 'checkbox' ? target.checked : target.value;
        let name = target.name;

        this.setState({
          [name]: value
        });
    }

    handleSubmit(e) {
        e.preventDefault();

        console.log('The form was submitted with the following data:');
        console.log(this.state);
        var account = {
          username : this.state.username,
          password: this.state.password
        }
        console.log(account)
       axios.post("http://localhost:8080/login", account)
        .then(res => {
          console.log(res.data)
          console.log(Number(res.data))
          if(Number(res.data) == 0 || Number(res.data) == 1 || Number(res.data) == 2)
              this.setState({login : true,role:res.data})
        })
        .catch(err =>{
          console.log("Error");
        })
    }
   
    render() {
      if(this.state.login){
        if(this.state.role == 1)
            window.location.href="/patients"
        if(this.state.role == 2)
            window.location.href="/caregivers"
        if(this.state.role == 0)
            window.location.href="/doctor"
      }
        return (
        <div className="FormCenter">
            <form className="FormFields" onSubmit={this.handleSubmit}>
            <div className="FormField">
                <label className="FormField__Label" htmlFor="username">Username</label>
                <input type="username" id="username" className="FormField__Input" placeholder="Enter your username" name="username" value={this.state.username} onChange={this.handleChange} />
              </div>

              <div className="FormField">
                <label className="FormField__Label" htmlFor="password">Password</label>
                <input type="password" id="password" className="FormField__Input" placeholder="Enter your password" name="password" value={this.state.password} onChange={this.handleChange} />
              </div>

              <div className="FormField">
                  {/* <button className="FormField__Button mr-20">Sign In</button>  */}
                  <input type="button" id="button1" value="Sign in" onClick={this.handleSubmit}/*onClick='redirectToURL(this.id)'*//>
              </div>
            </form>
          </div>
        );
    }
}

export default LoginForm;