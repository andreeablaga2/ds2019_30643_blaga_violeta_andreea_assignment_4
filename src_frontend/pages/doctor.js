import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router'
import axios from "axios";
import Button from "react-bootstrap/Button";
import { browserHistory } from 'react-router'

function patientRedirect(){
    browserHistory.push("/patients");
}
 
function caregiverRedirect(){
    browserHistory.push("/caregivers");
}
 
function medicationRedirect(){
    browserHistory.push("/medication");
}

function medicationPlanRedirect(){
    browserHistory.push("/medication");
}

function patientHistoryRedirect(){
  browserHistory.push("/history");
}

class Doctor extends Component{
  constructor(){
    super();
  }

  handleChange(e){

  }

  handleSubmit(e){

  }

  render(){
    return (
        <form onSubmit={this.handleSubmit}>
           
        <p></p>
       
          <Button onClick={caregiverRedirect}
                  variant = "success"
                  type = {"submit"}
          >CRUD Caregivers</Button>

          <p></p>
          <Button onClick={patientRedirect}
                  variant = "success"
                  type = {"submit"}
          >CRUD Patients</Button>

          <p></p>
          <Button onClick={medicationRedirect}
                  variant = "success"
                  type = {"submit"}
          >CRUD Medication</Button>

          <p></p>
          <Button onClick={medicationRedirect}
                  variant = "success"
                  type = {"submit"}
          >Create MedicationPlan</Button>

          <p></p>
          <Button onClick={patientHistoryRedirect}
                  variant = "success"
                  type = {"submit"}
          >View Patient History</Button>

      </form>
    );
  }


}

export default Doctor;