import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router'
import axios from "axios";
import Button from "react-bootstrap/Button";
import { browserHistory } from 'react-router'
import {Chart} from 'primereact/chart';
import {DataTable} from 'primereact/datatable';
import {InputText} from 'primereact/inputtext';
import Column from 'primereact/datatable';

function add() {
  alert("Recommendation added!");
}


class History extends Component{
    constructor(){
        super();
        this.state={
           /* monitoredData:{
              data_id:"",
              patientID:"",
              start:"",
              end:"",
              activity:""
            },*/
            monitoredData:[],
            activitiesArray:[],
            value: null,
            medicationArray:[],
            patientID:null,
            date:null,
            meds:[],
        }
      }

    getMonitorData(){
      axios.get("http://localhost:8080/doctor/1/history", this.state)
            .then(response => {
                this.setState({
                    monitoredData: response.data
 
                })
 
                //console.log(this.state.monitoredData)
            }).catch(() =>
                console.log("Eroare")
            )
    }
    componentDidMount(){
      this.getMonitorData();
      var med1={
        name:"",
        date:"",
        patientID:"",
        taken:""
        };
        var med2={
            name:"",
            date:"",
            patientID:"",
            taken:""
            };
            var med3={
                name:"",
                date:"",
                patientID:"",
                taken:""
                };
      var array=[];
      med1.name="Lokren";
      med1.date="2019-12-03";
      med1.patientID="1";
      med1.taken="Taken";
      array.push(med1);
      med2.name="Accupro";
      med2.date="2019-12-03";
      med2.patientID="1";
      med2.taken="Not Taken";
      array.push(med2);
      med3.name="Nurofen";
      med3.date="2019-12-04";
      med3.patientID="1";
      med3.taken="Not Taken";
      array.push(med3);
      console.log(array);
      this.setState({medicationArray:array});

    }

     check() {
        var  medss=[];
        console.log(this.state.medicationArray);
        console.log(this.state.date);
        for (let i = 0, size = this.state.medicationArray.length; i < size; i++) {
           // console.log(this.state.medicationArray[i].date);
            if(this.state.medicationArray[i].date.localeCompare("2019-12-03")===0){
                console.log("khduquk");
                medss.push(this.state.medicationArray[i]);
            }
        }
       this.setState({meds:medss});
      }

    handleChange(e){
    
    }
    
    handleSubmit(e){
    
    }
    chartData() {
      let sleep = 0;
      let toilet = 0;
      let shower = 0;
      let breakfast = 0;
      let grooming = 0;
      let spare = 0;
      let leaving = 0;
      let lunch = 0;
      let snack = 0;
      for (let i = 0, size = this.state.monitoredData.length; i < size; i++) {
          var item = this.state.monitoredData[i].activity;
          if (item.includes("Sleeping")) {
              sleep = sleep + 1;
          }
          else if (item.includes("Toileting")) {
             // console.log(toilet)
              toilet = toilet + 1;
          }
          else if (item.includes("Showering")) {
              shower = shower + 1;
          }
          else if (item.includes("Breakfast")) {
              breakfast = breakfast + 1;
          }
          else if (item.includes("Grooming")) {
              grooming = grooming + 1;
          }
          else if (item.includes("Spare_Time/TV")) {
              spare = spare + 1;
          }
          else if (item.includes("Leaving")) {
              leaving = leaving + 1;
          }
          else if (item.includes("Lunch")) {
              lunch = lunch + 1;
          }
          else if (item.includes("Snack")) {
              snack = snack + 1;
          }
         // console.log(item)
      }
     
      this.state.activitiesArray[0] = sleep;
      this.state.activitiesArray[1] = toilet;
      this.state.activitiesArray[2] = shower;
      this.state.activitiesArray[3] = breakfast;
      this.state.activitiesArray[4] = grooming;
      this.state.activitiesArray[5] = spare;
      this.state.activitiesArray[6] = leaving;
      this.state.activitiesArray[7] = lunch;
      this.state.activitiesArray[8] = snack;
  }

   
    render(){
      this.chartData();

      const data = {
        labels: ["Sleeping","Toileting","Showering","Breakfast", "Grooming", "Spare_Time/TV","Leaving","Lunch","Snack"],
        datasets: [
            {
                data: [this.state.activitiesArray[0],
                this.state.activitiesArray[1],
                this.state.activitiesArray[2],
                this.state.activitiesArray[3],
                this.state.activitiesArray[4],
                this.state.activitiesArray[5],
                this.state.activitiesArray[6],
                this.state.activitiesArray[7],
                this.state.activitiesArray[8]],
                backgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                    "#FFCE56",
                    "#33ffd6",
                    "#800000",
                    "#80ff80",
                    "#ffff1a",
                    "#cc0044",
                    "#8080ff"
                ],
                hoverBackgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                    "#FFCE56",
                    "#33ffd6",
                    "#800000",
                    "#80ff80",
                    "#ffff1a",
                    "#cc0044",
                    "#8080ff"
                ]
            }]    
        };
       return(
       <div>
              {/* <div className="content-section introduction">
                  <div className="feature-intro">
                      <h1>DoughnutChart</h1>
                      <p>A doughnut chart is a variant of the pie chart, with a blank center allowing for additional information about the data as a whole to be included.</p>
                  </div>
              </div> */}
              <br></br>
              <br></br>
              <div className="content-section implementation">
                  <Chart type="doughnut" data={data} />
              </div>
              <br></br>
            
              <br></br>
             
            <div style={{paddingLeft:"150px"}}>
            <label> dataID: &nbsp; </label><br/>
            <InputText value={this.state.monitoredData.dataID} onChange={(e) => this.setState({value: e.target.value})} />
            <br/><br/>
            <label> activity: &nbsp; </label><br/>
            <InputText value={this.state.monitoredData.activity} onChange={(e) => this.setState({value: e.target.value})} />
            <br/><br/>
            <label> normal: &nbsp; </label><br/>
            <InputText value={this.state.monitoredData.start} onChange={(e) => this.setState({value: e.target.value})} />
            <br/><br/>
            <label> recommendation: &nbsp; </label><br/>
            <InputText value={this.state.monitoredData.end} onChange={(e) => this.setState({value: e.target.value})} />
            </div>
            <div style={{paddingTop:"30px",paddingLeft:"190px"}}>
            <Button  onClick={add}
                  variant = "success"
                  type = {"submit"}
          >Add</Button>
          
          </div>

          <div style={{paddingLeft:"150px",paddingTop:"30px"}}>
            <label> Patient ID: &nbsp; </label><br/>
            <InputText value={this.state.patientID} onChange={(e) => this.setState({value: e.target.value})} />
            <br/><br/>
            <label> Date: &nbsp; </label><br/>
            <InputText value={this.state.date} onChange={(e) => this.setState({value: e.target.value})} />
            <br/><br/>
           </div>
            <div style={{paddingTop:"30px",paddingLeft:"190px"}}>
            <Button  onClick={() => this.check()}
                  variant = "success"
                  type = {"submit"}
          >Check</Button>
          
          </div>

          <br/><br/>
            <div>
            <table class="table table-dark">
                    <thead>
                        <tr>
                        <th scope="col">name</th>
                        <th scope="col">date</th>
                        <th scope="col">taken</th>
                        </tr>
                    </thead>
                    {this.state.meds && this.state.meds.length !== 0 && (
                   <div> {this.state.meds.map(function(m) {
                      return (
                       
                         
                          <tbody>
                            <tr>
                              <td className="bl">{m.name}</td>
                              <td className="bl">{m.date}</td>
                              <td className="bl">{m.taken}</td>
                           
                            </tr>
                          </tbody>
                        
                     );
                    }, this)}
                    </div>
                    )}
                    </table>
            </div>
          

            <br/><br/>
            <div>
            <table class="table table-dark">
                    <thead>
                        <tr>
                        <th scope="col">patient id</th>
                        <th scope="col">start</th>
                        <th scope="col">end</th>
                        <th scope="col">activity</th>
                        </tr>
                    </thead>
                    
                    {this.state.monitoredData.map(function(activity) {
                      return (
                       
                         
                          <tbody>
                            <tr>
                              <td className="bl">{activity.patientID}</td>
                              <td className="bl">{activity.start}</td>
                              <td className="bl">{activity.end}</td>
                              <td className="bl">{activity.activity}</td>
                            </tr>
                          </tbody>
                        
                     );
                    }, this)}
                 
                    </table>
            </div>
          


        </div>);

    }
}

export default History;